//Local MongoDB Connection
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

// var url = "mongodb+srv://aidasani:qwertyuiop3@tempcluster.avymb.mongodb.net/?retryWrites=true&w=majority";


MongoClient.connect(url, function(err, db) 
{
  if (err) 
  	throw err;
  var myDB = db.db("TokenKaroNa");
  var myobj = [
  				{ _id:1 ,place: "Santa Monica Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:2 ,place: "Venice Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:3 ,place: "Long Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:4 ,place: "Malibu Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:5 ,place: "Manhattan Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:6 ,place: "Redondo Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:7 ,place: "Hermosa Beach", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:8 ,place: "El Matador", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:9 ,place: "El Segundo", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:10 ,place: "Lake Balboa", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:11 ,place: "Lake Hughes", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:12 ,place: "Griffith Observatory", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:13 ,place: "LACMA", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:14 ,place: "Natural History Museum", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:15 ,place: "Eaton Canyon", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  				{ _id:16 ,place: "The Grove", weeklyTokens:0, dailyTokens:0,currentTokens:0, url1:"",url2:"",url3:"",url4:"",url5:"",
          description: ""},
  			];
  myDB.collection("places").insertMany(myobj, function(err, res) 
  {
	  if (err) 
	    throw err;
	  console.log("Number of documents inserted: " + res.insertedCount);
	  db.close();
  });
});