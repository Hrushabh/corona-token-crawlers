import json
import requests
from bs4 import BeautifulSoup

URL = 'http://publichealth.lacounty.gov/media/coronavirus/locations.htm'
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')
td_elements = soup.find_all('td')
with open('graphData.json') as f:
	data = json.load(f)

counterWeekly = data['Weekly']
race = {}
ageGroup = {}
basic = [0]*4
i=0
flag = [False]*15
secondFlag = True
def writeToJson(basic, race, ageGroup):
	data['TotalCases'] = basic[2]
	data['TotalDeaths'] = basic[3]
	data['DailyCases'][counterWeekly%7] = basic[0]
	data['DailyDeaths'][counterWeekly%7] = basic[1]
	data['AgeGroup'] = ageGroup
	data['Race'] = race
	data['Weekly'] += 1
	with open('graphData.json', 'w') as json_file:
		json.dump(data, json_file)

# if data['counterWeekly'] == 0:
count =0
for td_text in td_elements:
	if flag[0] or td_text.text == "Cases**":
		if flag[0] == False:
			flag[0] = True
		else:
			basic[i] = int(td_text.text)
			i=i+1
			flag[0] = False

	if flag[1] or (td_text.text == "Deaths" and secondFlag):
		if flag[1] == False:
			flag[1] = True
		else:
			basic[i] = int(td_text.text)
			i=i+1
			flag[1] = False

	if flag[2] or td_text.text == "Laboratory Confirmed Cases ":
		if flag[2] == False:
			flag[2] = True
		else:
			basic[i] = int(td_text.text)
			i=i+1
			flag[2] = False

	if flag[3] or td_text.text == 'Deaths':
		if secondFlag == True:
			secondFlag = False
		else:
			if flag[3] == False:
				flag[3] = True
			else:
				basic[i] = int(td_text.text)
				flag[3] = False

	########################################################################################################

	if flag[4] or td_text.text == "-  0 to 17":
		if flag[4] == False:
			flag[4] = True
		else:
			ageGroup["0 - 17"] = int(td_text.text)
			flag[4] = False

	if flag[5] or td_text.text == "-  18 to 40":
		if flag[5] == False:
			flag[5] = True
		else:
			ageGroup["18 - 40"] = int(td_text.text)
			flag[5] = False

	if flag[6] or td_text.text == "-  41 to 65":
		if flag[6] == False:
			flag[6] = True
		else:
			ageGroup["41 - 65"] = int(td_text.text)
			flag[6] = False

	if flag[7] or td_text.text == '-  over 65':
		if flag[7] == False:
			flag[7] = True
		else:
			ageGroup["over 65"] = int(td_text.text)
			flag[7] = False

	########################################################################################################

	if flag[8] or td_text.text == "-  American Indian/Alaska Native":
		if flag[8] == False:
			flag[8] = True
		else:
			race["American Indian/Alaska Native"] = int(td_text.text)
			flag[8] = False

	if flag[9] or td_text.text == "-  Asian":
		if flag[9] == False:
			flag[9] = True
		else:
			race["Asian"] = int(td_text.text)
			flag[9] = False

	if flag[10] or td_text.text == "-  Black":
		if flag[10] == False:
			flag[10] = True
		else:
			race["Black"] = int(td_text.text)
			flag[10] = False

	if flag[11] or td_text.text == '-  Hispanic/Latino':
		if flag[11] == False:
			flag[11] = True
		else:
			race["Hispanic/Latino"] = int(td_text.text)
			flag[11] = False

	if flag[12] or td_text.text == "-  Native Hawaiian/Pacific Islander":
		if flag[12] == False:
			flag[12] = True
		else:
			race["Native Hawaiian/Pacific Islander"] = int(td_text.text)
			flag[12] = False

	if flag[13] or td_text.text == "-  White":
		if flag[13] == False:
			flag[13] = True
		else:
			race["White"] = int(td_text.text)
			flag[13] = False
			secondFlag = True

	if flag[14] or (td_text.text == '-  Other' and secondFlag):
		if flag[14] == False:
			flag[14] = True
		else:
			race["Other"] = int(td_text.text)
			flag[14] = False
			break

writeToJson(basic,race,ageGroup)
