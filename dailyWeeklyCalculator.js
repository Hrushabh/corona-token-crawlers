const fs = require('fs');

//Local MongoDB Connection
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var avgWeeklyRateDict={};

// function zeroGrowthRateHelper(idctr)
// {
// 	var ans=0;
// 	MongoClient.connect(url, function(err, db) 
// 	{
//   		if (err) 
//   			throw err;
//   		var myDB = db.db("TokenKaroNa");
//   		var myquery = { _id: idctr };
//   		myDB.collection("Tokens").find(myquery).toArray(function(err, result) 
//   		{
// 		    if (err) 
// 		    	throw err;
// 		    ans=result[0]["weeklyTokens"];
// 		    db.close();
//   		});
// 	});
// 	return ans;
// }

function weeklyWriteToDB(idctr,currentPlace,currentTokens)
{
	MongoClient.connect(url, function(err, db) 
	{
  		if (err) 
  			throw err;
  		var myDB = db.db("TokenKaroNa");
  		var myquery = { _id: idctr };
  		var myobj = {  $set: {weeklyTokens: currentTokens }};
  		myDB.collection("Tokens").updateOne(myquery, myobj, function(err, res) 
  		{
    		if (err) 
    			throw err;
    		console.log("1 document updated");
    		db.close();
  		});
	});
}

function dailyWriteToDB(idctr,currentPlace,currentTokens)
{
	MongoClient.connect(url, function(err, db) 
	{
  		if (err) 
  			throw err;
  		var myDB = db.db("TokenKaroNa");
  		var myquery = { _id: idctr };
  		var myobj = {  $set: {dailyTokens: currentTokens }};
  		myDB.collection("Tokens").updateOne(myquery, myobj, function(err, res) 
  		{
    		if (err) 
    			throw err;
    		console.log("1 document updated");
    		db.close();
  		});
	});
}

//JSON file read
var rawdata = fs.readFileSync('crawlerData.json');
var jsonData = JSON.parse(rawdata);
var weeklyData=jsonData.weekly;
var dailyData=jsonData.daily;

var currentPlace="";
var currentPopulation=0;
var currentMaxCap=0;
var currentAvgGrowthRate=0.0;
var currentTokens=0;
var idctr=0;

//Hyperparameters for weekly predictions formula
var k=0.25;
var m=1.5;

//*******************************************************ADD CONDITION SO THAT THIS IS DONE ONCE A WEEK***************************************************************************************
//For weekly predictions
for(var place in weeklyData) 
{
	idctr+=1;
	currentPlace=place;
	currentPopulation=jsonData.population[currentPlace];
	currentMaxCap=Math.round(currentPopulation/m);
	currentAvgGrowthRate=0;
	
	for(var day in weeklyData[currentPlace])
	{
		currentAvgGrowthRate+=weeklyData[currentPlace][day][1];
	}
	currentAvgGrowthRate/=7;
	currentAvgGrowthRate=Math.floor(currentAvgGrowthRate * 100) / 100;

	avgWeeklyRateDict[currentPlace]=currentAvgGrowthRate;
	
	currentTokens=Math.round(currentPopulation*Math.exp(-k*currentAvgGrowthRate));
	if(currentTokens>currentMaxCap)
	{
		currentTokens=currentMaxCap;
	}

	// console.log("Place: " +currentPlace);
	// console.log("population: " +currentPopulation);
	// console.log("Average: " +currentAvgGrowthRate); 
	// console.log("Tokens: " +currentTokens);

	weeklyWriteToDB(idctr,currentPlace,currentTokens);
}

var currentGrowthRate=0.0;
idctr=0;

//*******************************************************ADD CONDITION SO THAT THIS IS DONE DAILY***************************************************************************************
//For daily predictions
for(var place in dailyData) 
{
	idctr+=1;
	currentPlace=place;
	currentPopulation=jsonData.population[currentPlace];
	currentMaxCap=Math.round(currentPopulation/m);
	currentGrowthRate=0;
	
	if(jsonData.counterDaily%2==0)
		currentGrowthRate=dailyData[currentPlace]["1"][1];
	else
		currentGrowthRate=dailyData[currentPlace]["0"][1];
	
	if(currentGrowthRate==0)
	{
		currentGrowthRate=avgWeeklyRateDict[currentPlace];
	}

	currentGrowthRate=Math.floor(currentGrowthRate * 100) / 100;
	
	currentTokens=Math.round(currentPopulation*Math.exp(-k*currentGrowthRate));

	if(currentTokens>currentMaxCap)
	{
		currentTokens=currentMaxCap;
	}

	// console.log("Place: " +currentPlace);
	// console.log("population: " +currentPopulation);
	// console.log("Average: " +currentGrowthRate); 
	// console.log("Tokens: " +currentTokens);

	dailyWriteToDB(idctr,currentPlace,currentTokens);
}