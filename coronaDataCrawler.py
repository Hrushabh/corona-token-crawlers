import json
import requests
from bs4 import BeautifulSoup

URL = 'http://publichealth.lacounty.gov/media/coronavirus/locations.htm'
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')
td_elements = soup.find_all('td')

cases = [0] * 16
data = 0
cities = ["Santa Monica Beach","Venice Beach","Long Beach","Malibu Beach","Manhattan Beach","Redondo Beach","Hermosa Beach","El Matador","El Segundo","Lake Balboa","Lake Hughes","Griffith Observatory","LACMA","Natural History Museum","Eaton Canyon","The Grove"]

with open('crawlerData.json') as f:
	data = json.load(f)
if data['counterDaily'] == 0:
	for i in cities:
		data['weekly'][i] = {}
		data['daily'][i] = {}
counterDaily = data['counterDaily']
counterWeekly = data['counterWeekly']

long_beach = ["City of Signal Hill", "City of Lakewood", "Los Angeles - Wilmington"]

lake_hughes = ["Unincorporated - Palmdale","Unincorporated - Lake Hughes"]

griffith = ["City of Glendale", "City of Burbank", "Los Angeles - Hollywood", "Los Angeles - Downtown*"]

lacma = ["City of West Hollywood", "City of Beverly Hills", "Los Angeles - Downtown*","City of Culver City"]

nhm = ["Los Angeles - Exposition Park","Los Angeles - University Park","Los Angeles - Downtown*"]

eaton_canyon = ["Unincorporated - Altadena","City of South Pasadena","City of El Monte","City of Burbank"]

grove = ["Los Angeles - Melrose","City of Beverly Hills","Los Angeles - Downtown*","City of Culver City"]

flag = [False]*16

def writeToJson(cases,cities, counterWeekly):
	if counterWeekly == 0:			
		for i in range(len(cases)):
			data['weekly'][cities[i]][counterWeekly%7] = [cases[i], 0]
			data['daily'][cities[i]][counterDaily%2] = [cases[i], 0]
	else:
		for i in range(len(cases)):
			casesPrevWeekly = data['weekly'][cities[i]][str((counterWeekly-1)%7)][0]
			casePrevDaily = data['daily'][cities[i]][str((counterDaily-1)%2)][0]

			growthRateWeekly = round(((cases[i] - casesPrevWeekly)/float(casesPrevWeekly))*100,2)
			growthRateDaily = round(((cases[i] - casePrevDaily)/float(casePrevDaily))*100,2)
			data['weekly'][cities[i]][counterWeekly%7] = [cases[i], growthRateWeekly]
			data['daily'][cities[i]][counterDaily%2] = [cases[i], growthRateDaily]
	data['counterWeekly'] += 1
	data['counterDaily'] += 1
	with open('crawlerData.json', 'w') as json_file:
		json.dump(data, json_file)

for td_text in td_elements:
	#print(td_text)
	if flag[0] or td_text.text == "City of Santa Monica":
		if flag[0] == False:
			flag[0] = True
		else:
			cases[0] = int(td_text.text)
			flag[0] = False

	elif flag[1] or td_text.text == "Los Angeles - Venice":
		if flag[1] == False:
			flag[1] = True
		else:
			cases[1] = int(td_text.text)
			flag[1] = False

	elif flag[2] or td_text.text in long_beach:
		if flag[2] == False:
			flag[2] = True
		else:
			cases[2] += int(td_text.text)
			flag[2] = False

	elif flag[3] or td_text.text == "City of Malibu":
		if flag[3] == False:
			flag[3] = True
		else:
			cases[3] = int(td_text.text)
			flag[3] = False

	elif flag[4] or td_text.text == "City of Manhattan Beach":
		if flag[4] == False:
			flag[4] = True
		else:
			cases[4] = int(td_text.text)
			#print(cases[4])
			flag[4] = False

	elif flag[5] or td_text.text == "City of Redondo Beach":
		if flag[5] == False:
			flag[5] = True
		else:
			cases[5] = int(td_text.text)
			flag[5] = False

	elif flag[6] or td_text.text == "City of Hermosa Beach":
		if flag[6] == False:
			flag[6] = True
		else:
			cases[6] = int(td_text.text)
			flag[6] = False

	elif flag[8] or td_text.text == "City of El Segundo":
		if flag[8] == False:
			flag[8] = True
		else:
			cases[8] = int(td_text.text)
			flag[8] = False

	elif flag[9] or td_text.text == "Los Angeles - Lake Balboa":
		if flag[9] == False:
			flag[9] = True
		else:
			cases[9] = int(td_text.text)
			flag[9] = False

	elif flag[10] or td_text.text in lake_hughes:
		if flag[10] == False:
			flag[10] = True
		else:
			cases[10] += int(td_text.text)
			flag[10] = False

	elif flag[11] or td_text.text in griffith:
		if flag[11] == False:
			flag[11] = True
		else:
			cases[11] += int(td_text.text)
			flag[11] = False

	elif flag[12] or td_text.text in lacma:
		if flag[12] == False:
			flag[12] = True
		else:
			cases[12] += int(td_text.text)
			flag[12] = False

	elif flag[13] or td_text.text in nhm:
		if flag[13] == False:
			flag[13] = True
		else:
			cases[13] += int(td_text.text)
			flag[13] = False

	elif flag[14] or td_text.text in eaton_canyon:
		if flag[14] == False:
			flag[14] = True
		else:
			cases[14] += int(td_text.text)
			flag[14] = False

	elif flag[15] or td_text.text in grove:
		if flag[15] == False:
			flag[15] = True
		else:
			cases[15] += int(td_text.text)
			flag[15] = False
	cases[7] = cases[3]	
		
	######### if we want only los angeles area, change under investigation to "Unincorporated - Acton" ###########
	if td_text.text == "Unincorporated - Wiseburn":
		break

writeToJson(cases, cities, counterWeekly)

		